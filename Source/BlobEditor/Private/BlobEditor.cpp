// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlobEditor.h"
#include "UnrealEd.h"
#include "GroupBehaviourVisualizer.h"
#include "Blob/Boid/GroupBehaviour.h"

IMPLEMENT_GAME_MODULE(FBlobEditorModule, BlobEditor);

void FBlobEditorModule::StartupModule()
{
	if (GUnrealEd)
	{
		// Make a new instance of the visualizer
		TSharedPtr<FComponentVisualizer> visualizer = MakeShareable(new FGroupBehaviourVisualizer());

		// Register it to our specific component class
		GUnrealEd->RegisterComponentVisualizer(UGroupBehaviour::StaticClass()->GetFName(), visualizer);
		visualizer->OnRegister();
	}
}

void FBlobEditorModule::ShutdownModule()
{
	if (GUnrealEd)
	{
		// Unregister when the module shuts down
		GUnrealEd->UnregisterComponentVisualizer(UGroupBehaviour::StaticClass()->GetFName());
	}
}