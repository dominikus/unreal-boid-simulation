// Fill out your copyright notice in the Description page of Project Settings.


#include "GroupBehaviourVisualizer.h"
#include "Blob/Boid/GroupBehaviour.h"
#include "SceneManagement.h"

void FGroupBehaviourVisualizer::DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	const UGroupBehaviour* groupBehv = Cast<const UGroupBehaviour>(Component);
	if(!groupBehv)
		return;
	// Draw a cylinder around the object because why not
	DrawWireSphere(PDI,
		groupBehv->GetOwner()->GetActorLocation(),
		FLinearColor(0, 0, 1),	// Blue color
		groupBehv->GetRadius(),
		20,
		SDPG_Foreground,		// Will render on top of everything
		2.f						// Line width
	);

	for (auto& neighborBoid : groupBehv->GetBoidsInRadius())
	{
		if (neighborBoid)
		{
			PDI->DrawLine(
				groupBehv->GetOwner()->GetActorLocation(),
				neighborBoid->GetActorLocation(),
				FLinearColor(1.f, 0.f, 0.f),	// Red color
				SDPG_World,						// Will not render over other things
				2.0f);							// Line width
		}
	}
}
