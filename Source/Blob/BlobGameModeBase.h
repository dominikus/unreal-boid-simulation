// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlobGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BLOB_API ABlobGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
