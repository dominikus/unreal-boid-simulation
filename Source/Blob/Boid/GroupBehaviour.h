// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoidBehaviour.h"
#include "Components/SphereComponent.h"
#include "GroupBehaviour.generated.h"

/**
 * Swarm-, Flock-, Herd- and Group Behaviours which need to know their neighbors informations
 */
UCLASS(Abstract, BlueprintType)
class BLOB_API UGroupBehaviour : public UBoidBehaviour
{
	GENERATED_BODY()

private:
	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

	void AddSphere();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Boid|Behaviour")
		USphereComponent* m_SphereComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid|Behaviour")
		float m_Radius = 250.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Boid|Behaviour")
		TSet<ABoid*> m_BoidsInRadius;

	virtual void OnRegister() override;
	virtual void BeginPlay() override;
	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:
	TSet<ABoid*> GetBoidsInRadius() const;
	float GetRadius() const;
};
