// Fill out your copyright notice in the Description page of Project Settings.


#include "Cohersion.h"

FVector UCohersion::GetDirection()
{
	if (m_BoidsInRadius.Num() == 0) return FVector::ZeroVector;

	FVector centerOfMass = FVector::ZeroVector;
	for (ABoid* boid : m_BoidsInRadius)
	{
		centerOfMass += boid->GetActorLocation();
	}
	centerOfMass /= m_BoidsInRadius.Num();

	m_Importance = CalculateImportance(centerOfMass);

	return centerOfMass - m_Owner->GetActorLocation();
}

//TODO: Is the size of the vector a good measurement for cohersion?
float UCohersion::CalculateImportance(const FVector& InDirection)
{
	return (InDirection - m_Owner->GetActorLocation()).Size();
}
