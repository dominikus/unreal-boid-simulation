// Fill out your copyright notice in the Description page of Project Settings.


#include "Targeting.h"

FVector UTargeting::GetDirection()
{
	FVector direction = FVector::ZeroVector;
	direction = Target - m_Owner->GetActorLocation();

	m_Importance = CalculateImportance(direction);

	return direction;
}

float UTargeting::CalculateImportance(const FVector& InDirection)
{
	return (InDirection - m_Owner->GetActorLocation()).Size();
}
