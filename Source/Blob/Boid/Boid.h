// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Boid.generated.h"

class UBoidBehaviour;

USTRUCT(BlueprintType)
struct BLOB_API FBoidInfo
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid|Info")
		float SpeedMultiplier = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid|Info")
		float MaxAccel = 1.0f;
	UPROPERTY(VisibleAnywhere, Category = "Boid|Info")
		FVector Accel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid|Info")
		float MaxVeloc = 1.0f;
	UPROPERTY(VisibleAnywhere, Category = "Boid|Info")
		FVector Veloc;
	UPROPERTY(VisibleAnywhere, Category = "Boid|Info")
		FRotator LookDirection;
};


UCLASS(BlueprintType)
class BLOB_API ABoid : public AActor
{
	GENERATED_BODY()

private:

	/** First PrimitiveComponent of this actor to apply the forces to when UsePhysics is enabled.*/
	UPROPERTY()
		UPrimitiveComponent* m_PrimComp;

	FVector CalculateAcceleration(const TArray<UBoidBehaviour*>& Behaviours);
	UBoidBehaviour* GetMaxImportanceBehaviour(const TArray<UBoidBehaviour*>& Behaviours);
	void UpdateBoidInfo(float DeltaTime);
	void MoveBoid(float DeltaTime);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Boid")
		TArray<UBoidBehaviour*> BoidBehaviours;

	/**
	 * Use forces
	 * Else: Just change the position
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid")
		bool UsePhysics = false;

	/**
	 * Use a calculated importance of the behaviours to affect the boid
	 * Else: All behaviours affect the boid to the same amount each frame
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid")
		bool UseImportance = false;

	/**
	 * Only consider the behaviour with the highest importance
	 * Else: Use every behaviour
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid")
		bool OnlyMaxImportance = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid | Info")
		FBoidInfo BoidInfo;

	ABoid();
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
};
