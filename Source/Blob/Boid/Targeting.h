// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoidBehaviour.h"
#include "Targeting.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class BLOB_API UTargeting : public UBoidBehaviour
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid | Behaviour")
		FVector Target;

	FVector GetDirection() override;
	float CalculateImportance(const FVector& InDirection) override;
};
