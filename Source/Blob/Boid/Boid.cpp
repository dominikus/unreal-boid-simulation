// Fill out your copyright notice in the Description page of Project Settings.


#include "Boid.h"
#include "BoidBehaviour.h"


ABoid::ABoid()
{
	PrimaryActorTick.bCanEverTick = true;
}

UBoidBehaviour* ABoid::GetMaxImportanceBehaviour(const TArray<UBoidBehaviour*>& Behaviours)
{
	UBoidBehaviour* MaxImportanceBehaviour = Behaviours[0];
	float CurrentImportance = 0;

	for (UBoidBehaviour* Behaviour : Behaviours)
	{
		if (MaxImportanceBehaviour->GetImportance() < Behaviour->GetImportance())
			MaxImportanceBehaviour = Behaviour;
	}
	return MaxImportanceBehaviour;
}

void ABoid::UpdateBoidInfo(float DeltaTime)
{
	BoidInfo.Accel = CalculateAcceleration(BoidBehaviours);
	BoidInfo.Veloc = GetActorForwardVector();
	BoidInfo.Veloc += BoidInfo.Accel.GetClampedToMaxSize(BoidInfo.MaxAccel) * DeltaTime * BoidInfo.SpeedMultiplier;
	BoidInfo.LookDirection = BoidInfo.Veloc.GetSafeNormal().Rotation();
}

void ABoid::MoveBoid(float DeltaTime)
{
	if (UsePhysics && IsValid(m_PrimComp))
	{
		m_PrimComp->AddForce(BoidInfo.Veloc.GetClampedToMaxSize(BoidInfo.MaxVeloc), NAME_None, true);
		m_PrimComp->SetWorldRotation(BoidInfo.LookDirection);
	}
	else if(!UsePhysics)
	{
		this->SetActorLocationAndRotation(GetActorLocation() + BoidInfo.Veloc.GetClampedToMaxSize(BoidInfo.MaxVeloc), BoidInfo.LookDirection.Quaternion());
	}
}

FVector ABoid::CalculateAcceleration(const TArray<UBoidBehaviour*>& Behaviours)
{
	FVector accel = FVector::ZeroVector;

	if (UseImportance)
	{
		if (OnlyMaxImportance)
		{
			UBoidBehaviour* maxImpBehaviour = GetMaxImportanceBehaviour(Behaviours);
			accel = maxImpBehaviour->GetDirection() * maxImpBehaviour->GetWeight();
		}
		else
		{
			for (UBoidBehaviour* Behaviour : Behaviours)
			{
				if (IsValid(Behaviour))
					accel += Behaviour->GetDirection() * Behaviour->GetWeight() * Behaviour->GetImportance();
			}
		}
	}
	else
	{
		for (UBoidBehaviour* Behaviour : Behaviours)
		{
			if(IsValid(Behaviour))
				accel += Behaviour->GetDirection() * Behaviour->GetWeight();
		}
	}
	return accel;
}

void ABoid::BeginPlay()
{
	Super::BeginPlay();

	/** Fill the BoidBehaviours-Array with every on this actor added UBoidBehaviour */
	TInlineComponentArray<UBoidBehaviour*> Components(this);
	if (Components.Num() > 0)
	{
		for (UBoidBehaviour* Component : Components)
		{
			if (!IsValid(Component))
			{
				UE_LOG(LogTemp, Warning, TEXT("BoidBehaviour %s on Actor %s is not valid!"), *Component->GetClass()->GetFName().ToString(), *this->GetHumanReadableName())
				continue;
			}

			if (!BoidBehaviours.Contains(Component))
				BoidBehaviours.Add(Component);
			else
				UE_LOG(LogTemp, Log, TEXT("BoidBehaviour %s was already registered on Actor %s!"), *Component->GetClass()->GetFName().ToString(), *this->GetHumanReadableName())
		}
	}

	TInlineComponentArray<UPrimitiveComponent*> PrimComponents(this);
	if (PrimComponents.Num() > 0)
		m_PrimComp = PrimComponents[0];
}

void ABoid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (BoidBehaviours.Num() > 0)
	{
		UpdateBoidInfo(DeltaTime);
		MoveBoid(DeltaTime);
	}
}

#if WITH_EDITOR

void ABoid::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ABoid, UsePhysics)))
	{
		if(IsValid(m_PrimComp))
			m_PrimComp->SetSimulatePhysics(UsePhysics);
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif