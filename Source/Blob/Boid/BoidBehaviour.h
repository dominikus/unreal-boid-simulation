// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Boid.h"
#include "BoidBehaviour.generated.h"


UCLASS(Abstract, BlueprintType)
class BLOB_API UBoidBehaviour : public UActorComponent
{
	GENERATED_BODY()
	
protected:
	/** Importance of changing the direction */
	UPROPERTY(VisibleAnywhere, BlueprintGetter = GetImportance, Category = "Boid|Behaviour")
		float m_Importance;

	UPROPERTY()
		AActor* m_Owner;

	virtual void OnComponentCreated() override;
	virtual void BeginPlay() override;

public:
	/** Weight of this behaviour for the calculation of the boids steering */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid|Behaviour")
		float Weight = 1.0f;

	UFUNCTION()
		virtual FVector GetDirection() PURE_VIRTUAL(UBoidBehaviour::GetDirection, return FVector(0,0,0););

	UFUNCTION()
		virtual float CalculateImportance(const FVector& InDirection) PURE_VIRTUAL(UBoidBehaviour::CalculateImportance, return 1.0;);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Boid|Behaviour")
		float GetImportance();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Boid|Behaviour")
		float GetWeight();
};
