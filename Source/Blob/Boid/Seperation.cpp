// Fill out your copyright notice in the Description page of Project Settings.


#include "Seperation.h"

FVector USeperation::GetDirection()
{
	if (m_BoidsInRadius.Num() == 0) return FVector::ZeroVector;

	FVector seperationDirection = FVector::ZeroVector;
	for (ABoid* boid : m_BoidsInRadius)
	{
		seperationDirection += m_Owner->GetActorLocation() - boid->GetActorLocation();
	}
	seperationDirection /= m_BoidsInRadius.Num();

	m_Importance = CalculateImportance(seperationDirection);

	return seperationDirection;
}

float USeperation::CalculateImportance(const FVector& InDirection)
{
	return (InDirection - m_Owner->GetActorLocation()).Size();
}
