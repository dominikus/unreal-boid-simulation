// Fill out your copyright notice in the Description page of Project Settings.

#include "BoidBehaviour.h"


void UBoidBehaviour::OnComponentCreated()
{
	Super::OnComponentCreated();

	if (!GetOwner()->IsA<ABoid>())
	{
		UE_LOG(LogTemp, Warning, TEXT("The actor %s is not a UBoidBehaviour! Deleting this behaviour..."), *GetOwner()->GetHumanReadableName());
		this->UnregisterComponent();
		this->DestroyComponent();
	}
	else
	{
		m_Owner = GetOwner();

		TInlineComponentArray<UBoidBehaviour*> Components(m_Owner);
		if (Components.Num() > 0)
		{
			for (UBoidBehaviour* Component : Components)
			{
				if (Component->IsA(this->GetClass()) && Component != this)
				{
					UE_LOG(LogTemp, Warning, TEXT("The behaviour %s already exists on Actor %s! Deleting the new one..."), *this->GetClass()->GetFName().ToString(), *m_Owner->GetHumanReadableName());
					this->UnregisterComponent();
					this->DestroyComponent();
				}
			}
		}
	}
}

void UBoidBehaviour::BeginPlay()
{
	Super::BeginPlay();

	if (!IsValid(m_Owner))
		m_Owner = GetOwner();
}

float UBoidBehaviour::GetImportance()
{
	return FMath::Clamp(m_Importance, 0.0f, 1.0f);
}

float UBoidBehaviour::GetWeight()
{
	return FMath::Clamp(Weight, 0.0f, 1.0f);
}
