// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GroupBehaviour.h"
#include "Seperation.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class BLOB_API USeperation : public UGroupBehaviour
{
	GENERATED_BODY()
	
	FVector GetDirection() override;
	float CalculateImportance(const FVector& InDirection) override;
};
