// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GroupBehaviour.h"
#include "Cohersion.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class BLOB_API UCohersion : public UGroupBehaviour
{
	GENERATED_BODY()
	
	FVector GetDirection() override;
	float CalculateImportance(const FVector& InDirection) override;
};
