// Fill out your copyright notice in the Description page of Project Settings.


#include "Alignment.h"

FVector UAlignment::GetDirection()
{
	if (m_BoidsInRadius.Num() == 0) return FVector::ZeroVector;

	FVector alignmentDirection = FVector::ZeroVector;
	for (ABoid* boid : m_BoidsInRadius)
	{
		alignmentDirection +=boid->GetActorForwardVector();
	}
	alignmentDirection /= m_BoidsInRadius.Num();

	m_Importance = CalculateImportance(alignmentDirection);

	return alignmentDirection;
}

float UAlignment::CalculateImportance(const FVector& InDirection)
{
	return (InDirection - m_Owner->GetActorLocation()).Size();
}
