// Fill out your copyright notice in the Description page of Project Settings.

#include "GroupBehaviour.h"
#include "Engine/Engine.h"
#include "EngineUtils.h"

void UGroupBehaviour::OnRegister()
{
	Super::OnRegister();

	//if (!IsValid(m_SphereComp))
	//	AddSphere();
}

void UGroupBehaviour::BeginPlay()
{
	Super::BeginPlay();

	/** Allocate enough space for every boid in the world within the BoidsSet to not let it dynamically resize
	 * TODO: Create BoidManager and retrieve the max. amount of boids from there instead of counting for every GroupBehaviour
	 */ 
	if (IsValid(GetWorld()))
	{
		TActorIterator<ABoid> boidItr(GetWorld());
		int32 num = 0;
		while (boidItr)
		{
			++num;
			++boidItr;
		}

		/** minus 1 to not count oneself */
		m_BoidsInRadius.Reserve(num-1);
	}

	if (!IsValid(m_SphereComp))
		AddSphere();
}

void UGroupBehaviour::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	//if (IsValid(m_SphereComp))
	//{
	//	m_SphereComp->UnregisterComponent();
	//	m_SphereComp->DestroyComponent();
	//}

	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

void UGroupBehaviour::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->GetFName() == m_Owner->GetFName()) return;

	if (ABoid* boid = Cast<ABoid>(OtherActor))
	{
		if (m_BoidsInRadius.Contains(boid)) return;
		m_BoidsInRadius.Add(boid);
	}
}

void UGroupBehaviour::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if (OtherActor->GetFName() == m_Owner->GetFName()) return;

	if (ABoid* boid = Cast<ABoid>(OtherActor))
	{
		if (!m_BoidsInRadius.Contains(boid)) return;
		m_BoidsInRadius.Remove(boid);
	}
}

void UGroupBehaviour::AddSphere()
{
	FString name = this->GetName() + TEXT("_SphereComp");
	USphereComponent* sphereComponent = NewObject<USphereComponent>(this, *name);
	sphereComponent->RegisterComponent();	
	sphereComponent->AttachToComponent(GetOwner()->GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
	m_SphereComp = sphereComponent;
	sphereComponent->SetCollisionProfileName(TEXT("OverlapAll"));
	sphereComponent->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	sphereComponent->OnComponentBeginOverlap.AddDynamic(this, &UGroupBehaviour::BeginOverlap);
	sphereComponent->OnComponentEndOverlap.AddDynamic(this, &UGroupBehaviour::EndOverlap);
	sphereComponent->InitSphereRadius(m_Radius);
	sphereComponent->RecreatePhysicsState();
}

#if WITH_EDITOR

void UGroupBehaviour::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(UGroupBehaviour, m_Radius)))
	{
		if (IsValid(m_SphereComp))
		{
			m_SphereComp->SetSphereRadius(m_Radius);
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}

#endif

TSet<ABoid*> UGroupBehaviour::GetBoidsInRadius() const
{
	return *&m_BoidsInRadius;
}

float UGroupBehaviour::GetRadius() const
{
	return m_Radius;
}
